<br>

<br>

<p align='center'>
<b>English</b> | <a href="https://github.com/nekobc1998923/vitecamp/blob/master/README.zh-CN.md">简体中文</a>
</p>

<br>

## Features

- ⚡️ [Vue 3](https://github.com/vuejs/core), [Vite 3](https://github.com/vitejs/vite), [pnpm](https://pnpm.io/), [ESBuild](https://github.com/evanw/esbuild) - fast and faster !
- 💪 [Typescript](https://www.typescriptlang.org/) - of course! necessary
- 🎉 [Element Plus ready](https://github.com/element-plus/element-plus) - UI Library based on Vue.js 3
- 🔥 [Axios](https://github.com/axios/axios) - Http Library based on Promise
- 💡 [Vue Router 4](https://router.vuejs.org/zh/) - The official router for Vue.js
- 📦 [Components auto importing](https://github.com/antfu/unplugin-vue-components) - Automatically register components on demand without import
- 📥 [Auto import APIs](https://github.com/antfu/unplugin-auto-import) - Automatically import APIs
- 🍍 [State Management via Pinia](https://pinia.esm.dev/) - The Vue Store that you will enjoy using
- 🎨 [Windi CSS](https://github.com/windicss/windicss) - next generation utility-first CSS framework
- 😃 [icones](https://github.com/antfu/unplugin-icons) - Powerful Icon Library, Use icons from any icon sets
- 🌍 [I18n ready](./locales) - Want to translate? Yes, you can!
- 👩‍🎨 [NProgress](https://github.com/rstacruz/nprogress) - Page loading progress feedback
- 😃 [SVG Support](https://github.com/jpkleemans/vite-svg-loader) - Support the use of SVG images as components
- 📑 [Markdown Support](https://github.com/antfu/vite-plugin-md) - Markdown as components / components in Markdown
- 🔑 Complete code style specification and code submission specification
- ☁️ Deploy on Netlify, zero-config

## Pre-packed

### UI Frameworks

- [Windi CSS](https://github.com/windicss/windicss) (On-demand [TailwindCSS](https://tailwindcss.com/)) - lighter and faster, with a bunch of additional features
  - [Windi CSS Typography](https://windicss.org/plugins/official/typography.html)
- [Element Plus](https://github.com/element-plus/element-plus) - A powerful Vue.js 3 UI Library

### Icons

- [🔍Icônes](https://icones.netlify.app/) - use icons from any icon sets
  - [unplugin-icons](https://github.com/antfu/unplugin-icons) - Automatically introduce the icons you need on demand

### Plugins

- [Vue Router 4](https://router.vuejs.org/zh/) - The official router for Vue.js
- [Pinia](https://pinia.esm.dev) - The Vue Store that you will enjoy using
- [Axios](https://github.com/axios/axios) - Http Library based on Promise
- [unplugin-vue-components](https://github.com/antfu/unplugin-vue-components) - components auto import
- [unplugin-auto-import](https://github.com/antfu/unplugin-auto-import) - Automatically import APIs
- [vite-plugin-windicss](https://github.com/antfu/vite-plugin-windicss) - Windi CSS Integration
- [vite-plugin-vue-markdown](https://github.com/antfu/vite-plugin-vue-markdown) - Markdown as components / components in Markdown
  - [markdown-it-prism](https://github.com/jGleitz/markdown-it-prism) - [Prism](https://prismjs.com/) for syntax highlighting
  - [prism-theme-vars](https://github.com/antfu/prism-theme-vars) - customizable Prism.js theme using CSS variables
  - [markdown-it-link-attributes](https://github.com/crookedneighbor/markdown-it-link-attributes) - Uniformly set the hyperlink jump mode in markdown
- [Vue I18n](https://github.com/intlify/vue-i18n-next) - Internationalization
  - [vite-plugin-vue-i18n](https://github.com/intlify/vite-plugin-vue-i18n) - Vite plugin for Vue I18n
- [vite-plugin-fonts](https://github.com/stafyniaksacha/vite-plugin-fonts) - Vite's font loader
- [VueUse](https://github.com/antfu/vueuse) - Collection of useful composition APIs
- [vite-svg-loader](https://github.com/jpkleemans/vite-svg-loader) - Support the use of SVG images as components

### Coding Style

- [ESLint](https://eslint.org/) with [Airbnb Style](https://github.com/airbnb/javascript)

### Dev tools

- [TypeScript](https://www.typescriptlang.org/)
- [Netlify](https://www.netlify.com/) - zero-config deployment
- [VS Code Extensions](./.vscode/extensions.json)
  - [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) -Vue 3 IDE support
  - [Iconify IntelliSense](https://marketplace.visualstudio.com/items?itemName=antfu.iconify) - Icon inline display and autocomplete
  - [i18n Ally](https://marketplace.visualstudio.com/items?itemName=lokalise.i18n-ally) - All in one i18n support
  - [Windi CSS Intellisense](https://marketplace.visualstudio.com/items?itemName=voorjaar.windicss-intellisense) - IDE support for Windi CSS
  - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Code quality and rule checking
  - [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - Focus on code formatting and beautifying code
  - [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) - Coding style check

## Try it now!

## How to contribute

You can [Raise an issue](https://gitee.com/taiping520/wit-admin/issues/new) Or submit a Pull Request.

**Pull Request:**

1. Fork code
2. Create your own branch: `git checkout -b feat/xxxx`
3. Submit your changes: `git commit -am 'feat(function): add xxxxx'`
4. Push your branch: `git push origin feat/xxxx`
5. submit `pull request`

## Git Contribution submission specification

- `feat` New features
- `fix` Fix bugs
- `docs` document
- `style` Format and style (changes that do not affect code operation)
- `refactor` Refactor
- `perf` Optimize related, such as improving performance and experience
- `test` Add test
- `build` Compilation related modifications, changes to project construction or dependencies
- `ci` Continuous integration modification
- `chore` Changes in the construction process or auxiliary tools
- `revert` Rollback to previous version
- `workflow` Workflow improvement
- `mod` Uncertain modification classification
- `wip` Under development
- `types` type

### GitHub Template

[Create a repo from this template on Gitee](https://gitee.com/taiping520/wit-admin.git).

### Clone to local

```bash
git clone https://github.com/nekobc1998923/vitecamp.git my-vitecamp-app
cd my-vitecamp-app
pnpm i
```

And then , you can enjoy coding fun :)

## Usage

### Development

Just run and visit http://localhost:8080

```bash
pnpm run dev
```

### Build

To build the App, run

```bash
pnpm run build
```

And you will see the generated file in `dist` that ready to be served.

## Thanks

This template has some features inspired by [Vitesse](https://github.com/antfu/vitesse) ❤
