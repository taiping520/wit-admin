import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { ElMessage } from 'element-plus';
import showCodeMessage from '@/api/code';
import { formatJsonToUrlParams, instanceObject } from '@/utils/format';
import NetConfig from '@/config/net';

// 创建实例
const axiosInstance: AxiosInstance = axios.create({
  // 前缀
  baseURL: NetConfig.baseURL,
  // 超时
  timeout: NetConfig.requestTimeout,
  // 请求头
  headers: {
    'Content-Type': NetConfig.contentType,
  },
});

// 请求拦截器
axiosInstance.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // TODO 在这里可以加上想要在请求发送前处理的逻辑
    // TODO 比如 loading 等

    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  },
);

// 响应拦截器
axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => {
    if (response.status in NetConfig.successCode) {
      return response.data;
    }
    ElMessage.info(JSON.stringify(response.data[NetConfig.messageName]));
    return response;
  },
  (error: AxiosError) => {
    const { response } = error;
    if (response) {
      ElMessage.error(showCodeMessage(response.data[NetConfig.statusName]));
      return Promise.reject(response.data);
    }
    ElMessage.warning('网络连接异常,请稍后再试!');
    return Promise.reject(error);
  },
);
const service = {
  get<T = never>(url: string, data?: object): Promise<T> {
    return axiosInstance.get(url, { params: data });
  },

  post<T = never>(url: string, data?: object): Promise<T> {
    return axiosInstance.post(url, data);
  },

  put<T = never>(url: string, data?: object): Promise<T> {
    return axiosInstance.put(url, data);
  },

  delete<T = never>(url: string, data?: object): Promise<T> {
    return axiosInstance.delete(url, data);
  },

  upload: (url: string, file: FormData | File) =>
    axiosInstance.post(url, file, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }),
  download: (url: string, data: instanceObject) => {
    window.location.href = `${NetConfig.baseURL}/${url}?${formatJsonToUrlParams(data)}`;
  },
};

export default service;
