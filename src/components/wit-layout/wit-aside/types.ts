/**
 * 菜单项字段配置结构
 */
export interface MenuOptions {
  title?: string;
  code?: string;
  icon?: string;
  children?: string;
}

/**
 * 菜单项默认字段名称
 */
export const defaultMenuOptions: MenuOptions = {
  title: 'title',
  code: 'code',
  icon: 'icon',
  children: 'children',
};

export interface MenuItemOptions {
  title?: string;
  id?: string;
  logo?: string;
  children?: string;
}

export const MenuItem: MenuItemOptions = {
  title: 'title',
  id: 'id',
  logo: 'logo',
  children: 'children',
};
