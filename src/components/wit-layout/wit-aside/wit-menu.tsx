import { DefineComponent, defineComponent, PropType } from 'vue';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import { defaultMenuOptions, MenuItemOptions, MenuOptions } from './types';

export default defineComponent({
  name: 'wit-menu',

  // 属性定义
  props: {
    data: {
      type: Array as PropType<MenuItemOptions[]>,
      required: true,
    },
    menuOptions: {
      type: Object as PropType<MenuOptions>,
      required: false,
      default: () => ({}),
    },
  },
  setup(props, context) {
    console.log(props, context);
    const { t } = useI18n();
    // 合并默认的字段配置和用户传入的字段配置
    const options = {
      ...defaultMenuOptions,
      ...props.menuOptions,
    };

    // 渲染图标
    const renderIcon = (icon?: string) => {
      if (!icon) {
        return null;
      }
      // h(`<${icon} />`);
      const Icons = (ElementPlusIconsVue as { [key: string]: DefineComponent })[icon];
      return (
        <el-icon>
          <Icons />
        </el-icon>
      );
      // return h(icon);
    };

    // 递归渲染菜单
    const renderMenu = (list: any[]) => {
      return list.map((item) => {
        // 如果没有子菜单，使用 el-menu-item 渲染菜单项
        if (!item[options.children!] || !item[options.children!].length) {
          return (
            <el-menu-item index={item[options.code!]}>
              {renderIcon(item[options.icon!])}
              <span>{t(item[options.title!])}</span>
            </el-menu-item>
          );
        }

        // 有子菜单，使用 el-sub-menu 渲染子菜单
        // el-sub-menu 的插槽（title 和 default）
        const slots = {
          title: () => (
            <>
              {renderIcon(item[options.icon!])}
              <span>{t(item[options.title!])}</span>
            </>
          ),
          default: () => renderMenu(item[options.children!]),
        };

        return <el-sub-menu index={item[options.code!]} v-slots={slots} />;
      });
    };

    return () => <el-menu {...context.attrs}>{renderMenu(props.data)}</el-menu>;
  },
});
