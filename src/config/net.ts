import { Net } from './types';

const NetConfig: Net = {
  // 项目运行端口号
  port: 8000,
  // 项目运行接口，默认为 /mock-server
  // 真实项目需配置后台 API 接口，如: http://api.xxx.com
  baseURL: process.env.NODE_ENV === 'development' ? 'http://localhost:9999' : '',
  // 后台数据的接收方式application/json;charset=UTF-8 或 application/x-www-form-urlencoded;charset=UTF-8
  contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
  // 请求时间超时限时(ms)
  requestTimeout: 10000,
  // 请求成功返回的状态码，支持String、Array、int多种类型
  successCode: [200, 0, '200', '0'],
  // 请求状态的字段名称
  statusName: 'status',
  // 请求状态信息的字段名称
  messageName: 'message',
  // 不需要loading层的请求接口
  noLoading: [
    // '/user/selectUserByUserPass',
    // '/mail/addMail',
    // '/mail/addMail',
    // '/mail/getMailList',
    // '/mail/updateMailRead',
    // '/mailUser/getMailUserList',
  ],
  // WebSocket
  WebSocketURL: process.env.NODE_ENV === 'development' ? 'ws://localhost:10001/ws' : 'ws://148.70.21.198:10001/ws',
};

export default NetConfig;
