import { Theme } from './types';

/** Theme
 * 主题配置
 */
const ThemeConfig: Theme = {
  // 系统名称
  title: 'Wit-Admin' || import.meta.env.VITE_APP_TITLE,
  // 标题分隔符
  titleSeparator: ' - ',
  // 标题是否反转
  // 如果为false: "page - title"
  // 如果为ture : "title - page"
  titleReverse: false,
  // 面包屑分隔符，默认 /
  separator: '/',
  // 版本号
  version: '1.0.0',
  // 作者
  author: 'un',
  // logo名称，logo存在icon目录中
  logo: 'vue-fill',
  // 系统排列风格
  layout: 'vertical',
  // 主题色
  themeColor: 'default',
  // 头部固定
  headerFixed: true,
  // 显示页脚
  showFooter: true,
  // 标签是否开启
  tabs: true,
  // 标签风格
  tabsType: 'default',
  // 标签icon
  tabsIcon: true,
  // 面包屑路由跳转模式，默认 false
  breadCrumbReplace: false,
  // 显示进度条
  showProgress: true,
  // 显示徽标
  badge: true,
  // 圆点徽标
  badgeIsDot: false,
  // 加载文字
  loadingText: '正在拼命加载，请稍后... ...',
  // 加载样式 dot double-dot
  loadingSpinner: 'dot',
  // 消息持续时间 ms
  messageDuration: 5000,
  // 错误日志
  errorLog: ['development' /* , 'production' */],
  // 全屏按钮
  showFullscreen: true,
  // 消息按钮
  showMessage: true,
  // Debug按钮
  showDebug: true,
  // 主题设置按钮
  showTheme: true,
};

export default ThemeConfig;
