// 主题配置
export interface Theme {
  // 系统名称
  title: string;
  // 标题分隔符
  titleSeparator: string;
  // 标题是否反转
  // 如果为false: "page - title"
  // 如果为ture : "title - page"
  titleReverse: boolean;
  // 面包屑分隔符，默认 /
  separator: string;
  // 版本号
  version: string;
  // 作者
  author: string;
  // logo名称，logo存在icon目录中
  logo: string;
  // 系统排列风格
  layout: string;
  // 主题色
  themeColor: string;
  // 头部固定
  headerFixed: boolean;
  // 显示页脚
  showFooter: boolean;
  // 标签是否开启
  tabs: boolean;
  // 标签风格
  tabsType: string;
  // 标签icon
  tabsIcon: boolean;
  // 面包屑路由跳转模式，默认 boolean
  breadCrumbReplace: boolean;
  // 显示进度条
  showProgress: boolean;
  // 显示徽标
  badge: boolean;
  // 圆点徽标
  badgeIsDot: boolean;
  // 加载文字
  loadingText: string;
  // 加载样式 dot double-dot
  loadingSpinner: string;
  // 消息持续时间 ms
  messageDuration: number;
  // 错误日志
  errorLog: string[];
  // 全屏按钮
  showFullscreen: boolean;
  // 消息按钮
  showMessage: boolean;
  // Debug按钮
  showDebug: boolean;
  // 主题设置按钮
  showTheme: boolean;
}

// 网络配置
export interface Net {
  // 项目运行端口号
  port: number;
  // 项目运行接口，默认为 /mock-server
  // 真实项目需配置后台 API 接口，如: http://api.xxx.com
  baseURL: string;
  // 后台数据的接收方式application/json;charset=UTF-8 或 application/x-www-form-urlencoded;charset=UTF-8
  contentType: string;
  // 请求时间超时限时(ms)
  requestTimeout: number;
  // 请求成功返回的状态码，支持String、Array、int多种类型
  successCode: never[];
  // 请求状态的字段名称
  statusName: string;
  // 请求状态信息的字段名称
  messageName: string;
  // 不需要loading层的请求接口
  noLoading: string[];
  // WebSocket
  WebSocketURL: string;
}
